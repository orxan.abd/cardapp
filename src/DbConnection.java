package src;

import java.sql.*;

public class DbConnection {
    private static final String DB_URL = "jdbc:mysql://localhost:3306/CardApp";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "Orx@n123root";

    public static Connection dbConnection;
    public static PreparedStatement insertClientStatement;
    public static PreparedStatement getProductsStatement;
    public static PreparedStatement insertBoughtProductStatement;
    public static PreparedStatement getBoughtProductsStatement;
    public static PreparedStatement getProductsQuantityStatement;
    public static PreparedStatement getClientInfoStatement;

    public static void databaseConnection(){
        try {
            dbConnection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public static void prepareStatements(){
        String insertClientSql = "INSERT INTO clients (ClientName, ClientSurName, ClientAge,CardNum, CardPin, Balance) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            insertClientStatement = dbConnection.prepareStatement(insertClientSql, Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        String getProductsSql = "SELECT * FROM Products";
        try {
            getProductsStatement = dbConnection.prepareStatement(getProductsSql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        String insertBoughtProductSql = "INSERT INTO BoughProducts (ClientId, ProductName, Quantity, Price) VALUES (?, ?, ?, ?)";
        try {
            insertBoughtProductStatement = dbConnection.prepareStatement(insertBoughtProductSql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        String getBoughtProductsSql = "SELECT * FROM BoughProducts WHERE ClientId = ?";
        try {
            getBoughtProductsStatement = dbConnection.prepareStatement(getBoughtProductsSql);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        String getProductsQuantity = "SELECT * FROM Products WHERE ProductId = ?";
        try {
            getProductsQuantityStatement = dbConnection.prepareStatement((getProductsQuantity));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        String getClientInfo = "SELECT * FROM Clients WHERE ClientId = ?";
        try {
            getClientInfoStatement = dbConnection.prepareStatement(getClientInfo);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}