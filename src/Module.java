package src;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class Module {

    public static void createNewCard(){
        DbConnection.databaseConnection();
        DbConnection.prepareStatements();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter PIN: ");
        String pin = scanner.nextLine();

        System.out.print("Confirm PIN: ");
        String confirmPin = scanner.nextLine();

        if (!pin.equals(confirmPin)) {
            throw new IllegalArgumentException("PINs are different!");
        }

        System.out.print("Enter name: ");
        String name = scanner.nextLine();

        System.out.print("Enter surname: ");
        String surname = scanner.nextLine();

        System.out.print("Enter age ");
        int age = scanner.nextInt();


        System.out.print("Enter balance: ");
        double balance = scanner.nextDouble();

        int cardNumber = generateCardNumber();

        try {
            DbConnection.insertClientStatement.setString(1, name);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            DbConnection.insertClientStatement.setString(2, surname);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            DbConnection.insertClientStatement.setInt(3, age);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            DbConnection.insertClientStatement.setInt(4, cardNumber);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            DbConnection.insertClientStatement.setString(5, pin);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            DbConnection.insertClientStatement.setDouble(6, balance);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            DbConnection.insertClientStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        ResultSet generatedKeys = null;
        try {
            generatedKeys = DbConnection.insertClientStatement.getGeneratedKeys();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        try {
            if (generatedKeys.next()) {
                int clientId = 0;
                try {
                    clientId = generatedKeys.getInt(1);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                System.out.println("New card created with Client ID: " + clientId);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static int generateCardNumber() {
        Random random = new Random();
        return random.nextInt(9000) + 1000; // Generate a 4-digit card number
    }

    public static void startShopping(){
        Scanner scanner = new Scanner(System.in);
        DbConnection.databaseConnection();
        DbConnection.prepareStatements();

        ResultSet productsResultSet = null;
        try {
            productsResultSet = DbConnection.getProductsStatement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Existing products:");

        while (true) {
            try {
                if (!productsResultSet.next()) break;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            int productId = 0;
            try {
                productId = productsResultSet.getInt("ProductId");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            String productName = null;
            try {
                productName = productsResultSet.getString("ProductName");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            int productQuantity = 0;
            try {
                productQuantity = productsResultSet.getInt("ProductQuantity");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            double price = 0;
            try {
                price = productsResultSet.getDouble("Price");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            System.out.println(productId + ". " + productName + " - Quantity: " + productQuantity + ", Price: " + price);
        }

        shoppingMessage();


    }

    public static void shoppingMessage () {
        System.out.println("Options:");
        System.out.println("1. Buy products");
        System.out.println("2. Show bought products");
        System.out.println("3. Exit");
        System.out.print("Enter your choice: ");
    }

    public static void shoppingChoice (){
        Scanner secScanner = new Scanner(System.in);
        int secChoice = 0;
        boolean secCorrectChoice = false;
        try {
            secChoice = secScanner.nextInt();
            secCorrectChoice = true;

            if (secChoice ==1) {
                buyProducts();
            } else if (secChoice ==2 ) {
                showBoughtProducts();
            } else if (secChoice == 3 ) {
                System.exit(0);
            } else if (secChoice < 1 || secChoice > 3 ) {
                throw new IllegalArgumentException("Invalid choice. Please enter a number between 1 and 3.");
            }
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter an integer");
            secScanner.nextLine();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }


    public static void welcomeMessage () {
        System.out.println("For entering new card info please enter 1: ");
        System.out.println("For start shopping please enter 2: ");
        System.out.println("For exit please enter 3: ");
    }

    public static void firstChoice () {
        Scanner scanner = new Scanner(System.in);
        int choice = 0;
        boolean correctInput = false;
        try {
            welcomeMessage();
            choice = scanner.nextInt();
            correctInput = true;

            if (choice ==1 ) {
                createNewCard();
            } else if ( choice == 2 ) {
                startShopping();
            } else if (choice == 3) {
                System.exit(0);
            } else if ( choice < 1 || choice > 3 ) {
                throw new IllegalArgumentException("Invalid choice. Please enter a number between 1 and 3.");
            }

        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please enter an integer");
            scanner.nextLine();
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void showBoughtProducts () {
        DbConnection.databaseConnection();
        DbConnection.prepareStatements();

        ResultSet productsResultSet = null;
        try {
            productsResultSet = DbConnection.getBoughtProductsStatement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Bought products:");

        while (true) {
            try {
                if (!productsResultSet.next()) break;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            int clientId = 0;
            try {
                clientId = productsResultSet.getInt("ClientId");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            String productName = null;
            try {
                productName = productsResultSet.getString("ProductName");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            int productId ;
            try {
                productId = productsResultSet.getInt("ProductId");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            int productQuantity = 0;
            try {
                productQuantity = productsResultSet.getInt("Quantity");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            double price = 0;
            try {
                price = productsResultSet.getDouble("Price");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }

            System.out.println(clientId + ". " + productName + " - Quantity: " + productQuantity + ", Price: " + price);
        }
    }

    public static void buyProducts (){
        DbConnection.databaseConnection();
        DbConnection.prepareStatements();
//        Connection connection = null;
//        PreparedStatement statement = null;
        Scanner thrScanner = new Scanner(System.in);
        System.out.println("Enter product quantity: ");
        int quantity = thrScanner.nextInt();
        System.out.println("Enter product id: ");
        int idPr = thrScanner.nextInt();


        ResultSet quantityResultSet = null;
        ResultSet balancaResultSet = null;

        try {
//            statement =connection.prepareStatement("SELECT * FROM Products where ProductName = ?");
          //  statement.setInt(1, quantity);
//            quantityResultSet = statement.executeQuery();
            DbConnection.getBoughtProductsStatement.setInt(1, idPr);
            quantityResultSet = DbConnection.getProductsQuantityStatement.executeQuery();
//            DbConnection.getBoughtProductsStatement.setInt(1, idPr);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        double pricePr;
        try {
            pricePr = quantityResultSet.getDouble("Price");
        } catch (SQLException e) {
            throw new RuntimeException();
        }

        try {
            if (quantityResultSet.next()) {
                int availableQuantity = quantityResultSet.getInt("ProductQuantity");
                if (quantity > availableQuantity) {
                    throw new IllegalArgumentException("Insufficent quantity of " + idPr);
                } else {
                    throw new IllegalArgumentException("Produt not found:");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            balancaResultSet = DbConnection.getClientInfoStatement.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException();
        }


        try {
            if (balancaResultSet.next()) {
                int availableBalance = balancaResultSet.getInt("Balance");
                System.out.println("You bought product:");
                if ((quantity * pricePr) < availableBalance) {
                    throw new IllegalArgumentException("Insufficent balance");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

